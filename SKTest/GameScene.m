//
//  GameScene.m
//  SKTest
//
//  Created by Ian Hutchinson on 09/11/2015. OH please it was actually created by RYAN REID 
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    [self setBackgroundColor: [NSColor darkGrayColor]];
    self.rocketController = [[RocketController alloc] init];
    [self addChild: self.rocketController];
    
}

-(void) keyUp:(NSEvent *)theEvent {
    [self.rocketController handleKeyEvent: theEvent withKeyIsDown: NO];
}

-(void) keyDown:(NSEvent *)theEvent {
    [self.rocketController handleKeyEvent: theEvent withKeyIsDown: YES];
}

-(void)update:(CFTimeInterval) currentTime {
    /* Called before each frame is rendered */
    CFTimeInterval delta = currentTime - self.lastTime;
    [self.rocketController update: delta];
    self.lastTime = currentTime;
}



@end

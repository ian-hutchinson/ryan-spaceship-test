//
//  GameScene.h
//  SKTest
//

//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RocketController.h"

@interface GameScene : SKScene
{

}

@property (nonatomic, retain) RocketController* rocketController;
@property (nonatomic) CFTimeInterval lastTime;
@end

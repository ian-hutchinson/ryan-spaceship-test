//
//  main.m
//  SKTest
//
//  Created by Ian Hutchinson on 09/11/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}

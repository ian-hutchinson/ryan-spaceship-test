//
//  RocketController.m
//  SKTest
//
//  Created by Ian Hutchinson on 17/11/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#import "RocketController.h"
#import <Carbon/Carbon.h>

@implementation RocketController

-(id) init
{
    if (self = [super init])
    {
        self.pRocketSprite = [[SKSpriteNode alloc] initWithImageNamed: @"Spaceship"];
        [self.pRocketSprite setScale: 0.2f];
        
        
        self.pRocketSprite.physicsBody = [SKPhysicsBody bodyWithTexture: self.pRocketSprite.texture
                                                                   size: self.pRocketSprite.texture.size];
        
        self.pRocketSprite.physicsBody.affectedByGravity = NO;
        self.pRocketSprite.physicsBody.mass = 20.f;
        
        self.pRocketSprite.position = CGPointMake(CGRectGetMidX(self.frame),
                                                  CGRectGetMidY(self.frame));
        
        m_impulseStrength = 50.f;
        m_thrustStrength = 1000.f;
        m_thrustCooldownTimeSeconds = 2.f;
        m_thrustCooldown = 0.f;
        m_thrusting = NO;
        
//        NSString* firePath = [[NSBundle mainBundle] pathForResource:@"FireParticles" ofType:@"sks"];
//        self.pEmitter = [NSKeyedUnarchiver unarchiveObjectWithFile: firePath];
//        
//        [self.pEmitter setScale: 3.f];
//        
//        [self.pEmitter setParticleBirthRate: 0.f];
//        [self.pEmitter setPosition: CGPointMake(0.f, -150.f)];
//        [self.pEmitter setParticleSpeed: 400.f];
//        self.pEmitter.emissionAngleRange = 1.f;
        
//        [self.pRocketSprite addChild: self.pEmitter];
//        [self.pRocket addChild: self.pRocketSprite];
        
        [self addChild: self.pRocketSprite];
    }
    
    return self;
}

-(void) handleKeyEvent:(NSEvent*) theEvent withKeyIsDown:(BOOL)isDown
{
    unsigned short keyCode = [theEvent keyCode];

    const float rotationStrength = 0.05f;
    if (keyCode == kVK_Space)
    {
        if (!isDown)
        {
            m_thrusting = NO;
            return;
        }
        
        if ([self canApplyImpulse])
        {
            [self applyInitialImpulse];
        }
        
        [self applyThrust];
    }
    else if (keyCode == kVK_ANSI_A)
    {
        if (isDown)
        {
            [self.pRocketSprite.physicsBody applyAngularImpulse: rotationStrength];
        }
    }
    else if (keyCode == kVK_ANSI_D)
    {
        if (isDown)
        {
            [self.pRocketSprite.physicsBody applyAngularImpulse: -rotationStrength];
        }
    }
}

-(void) update:(CFTimeInterval) dt
{
    if (m_thrustCooldown < m_thrustCooldownTimeSeconds)
    {
        m_thrustCooldown += dt;
    }
}

- (void) applyInitialImpulse
{
    CGFloat rotation = self.pRocketSprite.zRotation + 1.5708;
    CGFloat dx = m_impulseStrength * cos(rotation);
    CGFloat dy = m_impulseStrength * sin(rotation);
    m_thrustCooldown = 0.f;
    
    [self.pRocketSprite.physicsBody applyImpulse: CGVectorMake(dx, dy)];
}

- (void) applyThrust
{
    m_thrusting = YES;
    NSLog(@"Apply thrust");
    CGFloat rotation = self.pRocketSprite.zRotation + 1.5708;
    CGFloat dx = m_thrustStrength * cos(rotation);
    CGFloat dy = m_thrustStrength * sin(rotation);
    
    [self.pRocketSprite.physicsBody applyForce: CGVectorMake(dx, dy)];
}

- (BOOL) canApplyImpulse
{
    return !m_thrusting && m_thrustCooldown >= m_thrustCooldownTimeSeconds;
}


@end

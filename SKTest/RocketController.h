//
//  RocketController.h
//  SKTest
//
//  Created by Ian Hutchinson on 17/11/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RocketController : SKNode
{
    BOOL m_thrusting;
    CGFloat m_thrustCooldown;
    CGFloat m_thrustCooldownTimeSeconds;
    CGFloat m_impulseStrength;
    CGFloat m_thrustStrength;
}

-(id) init;
-(void) handleKeyEvent:(NSEvent*) theEvent withKeyIsDown:(BOOL) isDown;
-(void) update:(CFTimeInterval) dt;

@property (nonatomic, retain) SKSpriteNode* pRocketSprite;
@property (nonatomic, retain) SKEmitterNode* pEmitter;

@end


